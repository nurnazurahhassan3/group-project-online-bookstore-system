<?php  
session_start();
include('admin/db_connect.php');
 ?>
<?php 
$book_id=$_POST['book_id'];
$book_qty=$_POST["qty"];

$sql = "select * from customer where CUST_ID= '".$_SESSION['id']."' ";
$result=mysqli_query($conn,$sql);
foreach($result->fetch_array() as $l => $v){
    $$l = $v;
}

if(isset($_SESSION['login'])){
	$qry = $conn->query("SELECT * from books where BOOK_ID = '".$book_id."' ");
	foreach($qry->fetch_array() as $k => $v){
		$$k = $v;
	}
}
$subtotal=$book_qty*$BOOK_PRICE;
$book_delivery =5;
$total=$subtotal+$book_delivery;
?>
<?php include 'header.php'?>
<?php include 'nav.php'?>
<div class="container-order">
    <div class="container-title1">
        <a class="close_modal" href="javascript:history.back()">
            <i class='bx bx-left-arrow-alt'></i><a>
                <h1>Check Out</h1>

    </div>
    <div class="delivery-address">
        <div class="delivery-title">
            <h2 class='bx bx-map'>
            </h2>
            <h3>Delivery Address
            </h3>
        </div>
        <div class="cust-address">
            <p><?php echo ucwords (strtolower($CUST_ADDRESS ))?></p>
        </div>
    </div>
    <div class="order-detail">
        <div class="book-detail1">
            <div class="book-pic">
                <img src="ADMIN SITE/image/<?php echo $IMAGE_PATH ?>" class="img-modal" alt="">
            </div>
            <div class="book-info">
                <div class="detail">
                    <p>Book Title: <b><?php echo $BOOK_TITLE ?></b></p>

                    <p>Price: <b>RM<?php echo number_format($BOOK_PRICE,2) ?></b></p>
                    <p>Quantity: <b><?php echo $book_qty ?></b></p>
                </div>
                <div class="subtotal">
                    <p>Subtotal: <b>RM<?php echo  number_format($subtotal,2)  ?></b></p>
                    <p>Delivery Cost: <b>RM<?php echo  number_format($book_delivery,2)  ?></b></p>
                </div>



            </div>
        </div>
        <div class="total">
            <p>Total:</p>
            <p><b>RM<?php echo  number_format($total,2)  ?></b></p>

        </div>

        <form class='submit-order' action="add_order.php" method="post">



            <input type="hidden" name="book_id" value=<?php echo $book_id?>>
            <input type="hidden" name="cust_id" value=<?php echo $_SESSION['id']?>>
            <input type="hidden" name="qty" value=<?php echo $book_qty?>>
            <input type="hidden" name="total_amount" value=<?php echo $total?>>
            <input type="hidden" name="order_status" value='PENDING'>
            <div class="btn-add-order">

                <a href="add_order.php"><button class='btn-checkout' type=' button'> Place
                        Order </button></a>


            </div>

        </form>


    </div>
</div>