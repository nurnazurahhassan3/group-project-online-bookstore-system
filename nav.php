<nav class="nav">

    <div class="navigation container">

        <div class="logo">
            <h1>BOOK 2U </h1>

        </div>

        <div class="menu">
            <div class="top-nav">
                <div class="logo">
                    <h1>Online Bookstore</h1>
                </div>
                <div class="close">
                    <i class='bx bx-x'></i>
                </div>
            </div>
            <ul class="nav-list">
                <li class="nav-item">
                    <a href="index.php" class="nav-link">Home</a>
                </li>

                <li class="nav-item">
                    <?php   
                if (!isset($_SESSION['login']) == 1){
                    echo " <a href='login_form.php' class='nav-link'>Account</a>";

                } else {
                    echo  " <a href='signup.php' class='nav-link'>Manage Account</a>";
                }
                ?>
                </li>

                <li class="nav-item">
                    <a href="#about" class="nav-link">About Us</a>
                </li>

                <?php   
                if (isset($_SESSION['login']) == 1){

                    echo " <li class='nav-item'>
                    <a href='logout.php?logout' class='nav-link'>Logout</a>
                </li>";

                }
                ?>

            </ul>
        </div>

        <a href="cart.html" class="cart-icon">
            <i class='bx bx-shopping-bag'></i>
        </a>

        <div class="hamburger">
            <i class="bx bx-menu"></i>
        </div>
    </div>
</nav>