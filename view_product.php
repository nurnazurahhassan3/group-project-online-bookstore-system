<?php 
session_start();
include 'admin/db_connect.php' ;
include 'header.php';
include 'nav.php';
?>
<?php

$book_id=$_GET['book_id'];

$qry = $conn->query("SELECT * , CATEGORY_NAME FROM books join categories using (CATEGORY_ID) where BOOK_ID= $book_id " );
foreach($qry->fetch_array() as $k => $val){
	$$k=$val;
}
?>


<div class="modal-container" id="uni_modal">
    <div class="modal-dialog">
        <div class="modal-content" id="modal_content" role='document'>
            <div class="modal-header">
                <a class="close_modal" href="index.php">
                    <i class='bx bx-left-arrow-alt'></i>
                </a>
                <h3 class="modal-title">View Book</h3>
            </div>
            <div class="modal-body">

                <div class="pic-part">
                    <img src="ADMIN SITE/image/<?php echo $IMAGE_PATH ?>" class="img-modal" alt="">
                </div>
                <div class="right-part">
                    <div class="detail-part">
                        <div class="title-book">
                            <p><b><?php echo $BOOK_TITLE ?></b></p>
                        </div>
                        <p><b><?php echo $BOOK_AUTHOR ?></b></p>
                        <p>Category: <b><?php echo $CATEGORY_NAME ?></b></p>
                        <p>Price: <b>RM<?php echo number_format($BOOK_PRICE,2) ?></b></p>
                        <p>Synopsis: </p>
                        <div class="book-detail">
                            <p><?php echo $BOOK_DESCRIPTION ?></p>
                        </div>
                    </div>
                    <div class="btn-part">
                        <form
                            action="<?php echo isset($_SESSION['login']) ? 'checkout.php' : "login_form.php?book_id=$book_id" ?>"
                            method="post">
                            <div class="btn-quatity">
                                <span class="btn-plus"><b><i class='bx bx-plus'></i></b></span>
                                <input type="number" name="qty" id="qty" value="1">
                                <span class="btn-minus"><b><i class='bx bx-minus'></i></b></span>
                            </div>
                            <div class="book-detail">
                                <input type="hidden" name="book_id" value=<?php echo $book_id ?>>
                            </div>
                            <div class="btn-add">
                                <button class='btn-cart btn-checkout' type=' button' value="buy">Buy</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
//const checkout = document.getElementById("checkout");

/*checkout.addEventListener("click", () => {
    modal_container.classList.remove("show");
});*/
$('.btn-plus').click(function() {
    var qty = $(this).siblings('input').val()
    qty = parseInt(qty) + 1;
    $(this).siblings('input').val(qty).trigger('change')
})
$('.btn-minus').click(function() {
    var qty = $(this).siblings('input').val()
    qty = parseInt(qty) - 1;
    $(this).siblings('input').val(qty).trigger('change')
})
</script>