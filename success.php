<?php 
session_start();
 include 'header.php';
 include 'nav.php';

?>
<div class="thank">
    <p>Thank You <i class='bx bx-happy-heart-eyes'></i></p>
</div>
<div class="container-success">

    <div class="order-place">
        <i class='bx bx-check-double'></i>
        <p>Your order has been placed!!</p>
    </div>

</div>