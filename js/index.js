window.onload = function () {
  const menu = document.querySelector(".menu");
  const navOpen = document.querySelector(".hamburger");
  const navClose = document.querySelector(".close");
  const navbar = document.querySelector(".nav");

  const navLeft = menu.getBoundingClientRect().left;

  navOpen.addEventListener("click", () => {
    if (navLeft < 0) {
      menu.classList.add("show");
      document.body.classList.add("show");
      navbar.classList.add("show");
    }
  });

  navClose.addEventListener("click", () => {
    if (navLeft < 0) {
      menu.classList.remove("show");
      document.body.classList.remove("show");
      navbar.classList.remove("show");
    }
  });
};
