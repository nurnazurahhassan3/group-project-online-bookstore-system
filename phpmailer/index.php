<?php
include'db_connect.php';
session_start();
$recipient =  $_SESSION["email"];
$name =  $_SESSION["name"];
$order_id = $_GET['order_ID'];
$sql = "select * from orders join books using (BOOK_ID
)  join customer using (CUST_ID
)where ORDER_ID= $order_id ";
$result=mysqli_query($conn,$sql);
foreach($result->fetch_array() as $l => $v){
    $$l = $v;
}
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

//Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_OFF;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'nurnazurahhassan4@gmail.com';                     //SMTP username
    $mail->Password   = 'nazurah0123';                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom('nurnazurahhassan3@gmail.com', 'BOOK 2U');
    $mail->addAddress($recipient, $name);     //Add a recipient
    //$mail->addAddress('ellen@example.com');               //Name is optional
    $mail->addReplyTo('nurnazurahhassan3@gmail.com','BOOK 2U');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
   // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

    //Content
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = 'Your BOOK 2U order has been received!';
    $mail->Body    = 
    "<div class='card w-100' >
    <div class='card-header bg-info'>
    <h3> Thank you for your order <h3>
    </div>
    
   <p> Hi $name,</p>

<p>Just to let you know, we have received your order <b>$order_id</b>, and it is now being processed.<p>
<h4> Your order is:</h4>
<p> Book Title: <b> $BOOK_TITLE </b></p>
<p> Book Price: <b>RM".number_format($BOOK_PRICE,2)." </b></p>
<p> Quantity: <b> $BOOK_QTTY </b></p>
<p> Delivery Cost: <b>RM".number_format(5,2)." </b></p>
<p> Total : <b>RM".number_format($TOTAL_AMOUNT,2)." </b></p>
<h4> Delivery Address:</h4>
<p>$CUST_ADDRESS</p>
    </div>";
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    header("location:../success.php"); 
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}