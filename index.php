<html lang="en">
<?php
        session_start();
        include('admin/db_connect.php');
?>


<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>BOOK 2U</title>

    <?php include 'header.php';?>




</head>

<body>
    <!--Header-->
    <header id="home" class="header">
        <!--Navigation-->
        <?php include 'nav.php'?>

        <!--Hero-->
        <div class="hero" id="hero">
            <img src="img/book.png" class="hero-img" alt="" />
            <div class="hero-content">
                <h1> WELCOME TO OUR
                    <span class="discount">ONLINE</span>
                    <span class="discount">BOOKSTORE</span>
                </h1>
                <h1>


                </h1>
                <a href="#" class="btn">shop now</a>
            </div>
        </div>

    </header>


    </header>
    <!--Featured product-->

    <section id="section featured">
        <div class="title">
            <?php
             $cid = isset($_GET['category']) ? $_GET['category'] : 0;
       if($cid>0 ){
        $title = "SELECT * from categories where CATEGORY_ID = '".$_GET['category']."' ";
        $result=mysqli_query($conn, $title);
        foreach($result->fetch_array() as $k => $v){
            $$k = $v;
        }

        $_title =  ucwords (strtolower($CATEGORY_NAME));
            echo "<h1>  $_title Books</h1> ";
        
    }
        else{
        echo "<h1>Books</h1> ";
     }
        ?>



        </div>



        <!--column-->
        <div class="product-row container">
            <div class="list-group">

                <form method="get" action="">
                    <div class="form-inline">
                        <label>Category:</label>

                        <select class="form-control" name="category">
                            <option value=0>All</option>

                            <?php
                            $category = $conn->query("SELECT * from categories order by CATEGORY_NAME asc");
                            while($row1=$category->fetch_assoc()):
                            ?>

                            <option value='<?php echo $row1['CATEGORY_ID'] ?>'>
                                <?php echo ucwords (strtolower($row1['CATEGORY_NAME'])) ?>
                            </option>
                            <?php endwhile; ?>
                        </select>

                        <button class="btn-filter btn-primary" type='submit'>
                            <i class='bx bx-filter-alt'></i>
                        </button>

                    </div>
                </form>
            </div>
            <div class="product-column ">
                <?php
                $cid = isset($_GET['category']) ? $_GET['category'] : 0;
              

                $where='';
                if($cid > 0){
                    $where = 'WHERE CATEGORY_ID='.$cid;
                }
                $books = $conn->query("SELECT * from books $where order by BOOK_TITLE asc");
                if($books->num_rows <= 0){
                    echo "<center><h2>No Available Product</h2></center>";
                } 
                while($row=$books->fetch_assoc()):
                ?>

                <div class="product">


                    <div class="product-header">

                        <img class="img-fluid" src="ADMIN SITE/image/<?php echo $row['IMAGE_PATH'] ?>"
                            alt="Card image cap">


                    </div>

                    <div class="product-footer">
                        <div class="footer-container">
                            <h3><b> <?php echo strtoupper ($row['BOOK_TITLE']) ?></b></h3>
                            <h3><?php echo $row['BOOK_AUTHOR'] ?></h3>
                            <h4 class="price">RM
                                <?php echo number_format($row['BOOK_PRICE']) ?>
                            </h4>

                        </div>

                        <form action='view_product.php' method="get">
                            <input type="hidden" name="book_id" value="<?php echo $row['BOOK_ID']?>"><br>
                            <button class="btn-product view_prod" type="submit" id="view_btn"
                                href="view_product.php">View
                            </button>
                        </form>

                    </div>


                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>









    <script src="js/index.js" \></script>

    <script>

    </script>
</body>

</body>

</html>