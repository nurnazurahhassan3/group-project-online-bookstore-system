-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3305
-- Generation Time: Jul 21, 2021 at 03:29 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `book_ordering_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `BOOK_ID` int(20) NOT NULL,
  `BOOK_AUTHOR` varchar(50) NOT NULL,
  `BOOK_TITLE` varchar(50) NOT NULL,
  `BOOK_PRICE` int(10) NOT NULL,
  `BOOK_VERSION` varchar(50) NOT NULL,
  `BOOK_PUBLISHDATE` date NOT NULL,
  `BOOK_DESCRIPTION` text NOT NULL,
  `IMAGE_PATH` text NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`BOOK_ID`, `BOOK_AUTHOR`, `BOOK_TITLE`, `BOOK_PRICE`, `BOOK_VERSION`, `BOOK_PUBLISHDATE`, `BOOK_DESCRIPTION`, `IMAGE_PATH`, `CATEGORY_ID`) VALUES
(1000, 'SUHAILA MAJID', '123 KEJAYAAN', 16, 'SECOND EDITION', '2020-12-31', 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally', '123kejayaan.jpg', 17),
(1001, 'INTAN ALI', 'THE GREAT GATSBY', 49, 'FIRST EDITION', '2021-11-06', 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan', 'greatgatsby.jpg', 15),
(1002, 'WAN NOOR', 'RESEPI 30 HARI', 51, 'FIRST EDITION', '2019-02-28', 'The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ in their grammar, their pronunciation and their most common words. Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators. To achieve this, it would be necessary to have uniform grammar, pronunciation and more common words. If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more', 'resepi30hari.jpg', 5),
(1003, 'JAMES DANE', 'KUNCI BERJAYA', 90, 'FIRST EDITION', '2020-04-04', '126 PAGES', 'kunciberjaya.jpg', 17),
(1004, 'FADZILAH KAMSAH', 'TIPS BELAJAR', 30, 'THIRD EDITION', '2018-09-09', 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. \"What\'s happened to me?\" he thought. It wasn\'t a dream. His room, a proper human', 'tipsbelajar.jpg', 17),
(1005, 'ABU BAKAR', 'INVISIBLE MAN', 13, 'FOURTH EDITION', '2021-07-09', 'The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk TV quiz. How quickly daft jumping zebras vex. Two driven jocks help fax my big quiz. Quick, Baz, get my woven flax jodhpurs! \"Now fax quiz Jack!\" my brave', 'invisibleman.jpg', 18),
(1006, 'MAYA HUSIN', 'COVID-19', 140, 'SECOND EDITION', '2021-03-06', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,', 'covid19.jpg', 1),
(1007, 'SUHANA KINA', 'FASHION TIPS', 75, 'THIRD EDITION', '2021-08-19', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'fashiontips.jpg', 19),
(1008, 'NUR AIZA ', 'HOW TO BE SUCCESS', 80, 'FIRST EDITION', '2021-07-29', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex', 'howtobesuccess.jpg', 17),
(1009, 'NUR AQILA', 'WEB PROGRAMMING', 45, 'FIFTH EDITION', '2021-01-01', 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan', 'webprogramming.jpg', 16);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `CATEGORY_ID` int(11) NOT NULL,
  `CATEGORY_NAME` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`CATEGORY_ID`, `CATEGORY_NAME`) VALUES
(1, 'EDUCATION'),
(3, 'HISTORICAL'),
(5, 'COOKING'),
(7, 'ISLAMIC'),
(14, 'BUSINESS'),
(15, 'FANTASY'),
(16, 'PROGRAMMING'),
(17, 'MOTIVATIONAL'),
(18, 'MYSTERY'),
(19, 'FASHION');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `CUST_ID` int(20) NOT NULL,
  `CUST_NAME` varchar(50) NOT NULL,
  `CUST_EMAIL` varchar(50) NOT NULL,
  `CUST_ADDRESS` varchar(200) NOT NULL,
  `CUST_TELLNO` varchar(11) NOT NULL,
  `CUST_PASS` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`CUST_ID`, `CUST_NAME`, `CUST_EMAIL`, `CUST_ADDRESS`, `CUST_TELLNO`, `CUST_PASS`) VALUES
(100, 'IKHWAN BIN ZAMBRI', 'ikhwan00@gmail.com', 'NO. 6 JALAN LANGKAWI SETAPAK 53300 KL', '113478900', 'IKHWAN10'),
(101, 'SITI SOFIA BINTI ABU BAKAR', 'sofia01@yahoo.com', 'NO.44, JALAN PANTAS, TAMAN CONNAUGHT 56000 CHERAS', '134500980', 'SOFIA99'),
(102, 'AISYAH BINTI DAUD', 'aisyah02@yahoo.com', 'UNIT NO. A-3A-01, BLOK A, DAMANSARA SURIA APARTMENT, KEPONG 52200 KUALA LUMPUR', '125390248', 'AISYAH13'),
(103, 'JAMILAH BINTI YUSOF', 'jamilah03@gmail.com', 'NO. 164, PERSIARAN ZAABA, TAMAN TUN DR ISMAIL 60000 KL', '173762937', 'JAMILAH56'),
(104, 'FAHRI BIN ZULKIFLI', 'fahri04@gmail.com', 'NO.15, JALAN 38/27B, TAMAN DESA SETAPAK 53300 KL', '137372674', 'FAHRI100'),
(105, 'KAMAL BIN ADLI', 'kamal05@yahoo.com', 'UNIT NO. 17-36-3 MAJESTIC TOWER, MONT KIARA PALMA, JALAN 1/170C, MONT KIARA 50480 KL', '123457838', 'KAMAL88'),
(106, 'SUFIAN BIN SAAD', 'sufian06@yahoo.com', 'NO.26-13A-3 BLOK 26 PRIMA MIDAH HEIGHTS, JALAN MIDAH 8, TAMAN MIDAH 56000 KL', '172528329', 'SUFIAN77'),
(107, 'KHATIJAH BINTI HALIM', 'katie07@yahoo.com', '8, LORONG PINGGIRAN BERTAM 23 , BERTAM PERDANA 4,13200 KEPALA BATAS, PENANG', '114768920', 'KHATIJAH44'),
(108, 'SITI NURHALIZA BINTI SULAIMAN', 'liza08@gmail.com', 'P-7-8 PANGSAPURI DESA PERMAI INDAH, JALAN HELANG, 11700 GELUGOR, PULAU PINANG', '125782936', 'HALIZA77'),
(109, 'AHMAD BIN KASIM', 'ahmad09@gmail.com', 'BLOK D-02-02, KONDOMINIUM MUTIARA, JALAN PERDA BARAT, BANDAR PERDA, 14000 BUKIT MERTAJAM, PENANG', '128937232', 'AHMAD23'),
(110, 'nazurah binti hassan', 'nur_nazurahhassan@yahoo.com ', 'NO.44, JALAN PANTAS, TAMAN CONNAUGHT 56000 CHERAS', '0137868999', '$2y$04$wOvoonzumOw2FNjSAvPgLeup4BBUiX4xDZOgnCg7LCu/Bl0AKCdUC');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `ORDER_ID` int(20) NOT NULL,
  `ORDER_STATUS` varchar(50) NOT NULL,
  `BOOK_ID` int(20) NOT NULL,
  `BOOK_QTTY` int(10) NOT NULL,
  `CUST_ID` int(20) NOT NULL,
  `TOTAL_AMOUNT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`ORDER_ID`, `ORDER_STATUS`, `BOOK_ID`, `BOOK_QTTY`, `CUST_ID`, `TOTAL_AMOUNT`) VALUES
(5010, 'PENDING', 1000, 1, 100, 0),
(5020, 'DELIVERED', 1001, 1, 101, 0),
(5030, 'DELIVERED', 1002, 1, 102, 0),
(5040, 'PENDING', 1003, 1, 103, 0),
(5050, 'PENDING', 1004, 1, 104, 0),
(5060, 'PENDING', 1005, 1, 105, 0),
(5070, 'DELIVERED', 1006, 1, 106, 0),
(5080, 'DELIVERED', 1007, 1, 107, 0),
(5090, 'DELIVERED', 1008, 1, 108, 0),
(5128, 'PENDING', 1009, 1, 110, 50),
(5129, 'PENDING', 1009, 3, 110, 140);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `PAYMENT_ID` int(20) NOT NULL,
  `PAYMENT_DATE` datetime NOT NULL,
  `PAYMENT_TOTAL` int(10) NOT NULL,
  `CUST_ID` int(20) NOT NULL,
  `ORDER_ID` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`PAYMENT_ID`, `PAYMENT_DATE`, `PAYMENT_TOTAL`, `CUST_ID`, `ORDER_ID`) VALUES
(2000, '2021-07-01 00:00:00', 16, 100, 5010),
(2015, '2021-07-20 15:15:46', 90, 110, 5129);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `PASSWORD` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `USERNAME`, `EMAIL`, `PASSWORD`) VALUES
(1, 'Nazurah', 'nur_nazurahhassan@yahoo.com', 'ac6234c57197486caf1c0d342dacc528');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`BOOK_ID`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`CATEGORY_ID`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`CUST_ID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`ORDER_ID`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`PAYMENT_ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `BOOK_ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1010;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `CATEGORY_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `CUST_ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `ORDER_ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5130;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `PAYMENT_ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2016;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
