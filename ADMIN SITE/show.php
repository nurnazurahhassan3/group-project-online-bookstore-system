<?php
    require_once('db_connect.php');

    $sql = "SELECT * FROM `books`";
    $result = $conn->query($sql);

?>
<div>
    <div class="jumbotron">
        <h1 class="text-center">
            BOOK LIST
        </h1>
    </div>

    <div class="container">
        <table class="table table-striped table-borderrer">
            <tr>
                <th>ID#</th>
                <th>Image</th>
                <th>Details</th>
                <th>Price(RM)</th>
                <th>Actions</th>
            </tr>

            <?php
        if($result->num_rows > 0 ){
            while($row = $result->fetch_assoc()){
                echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['image'] . "</td>";
                echo "<td> Rs. " . $row['price'] . "</td>";
                echo "<td>" . $row['details'] . "</td>";
                echo "<td>";
                echo "<div class='btn-group'>";
                echo "<a class='btn btn-secondary' href='./update.php?id=" .$row['id'] ."'> Update </a>";
                echo "<a class='btn btn-danger' href='./delete.php?id=" .$row['id'] ."'> Delete</a>";
                echo "</div>";
                echo "</td>";
                echo "</tr>";
            }
        }
    ?>
        </table>

    </div>
</div>