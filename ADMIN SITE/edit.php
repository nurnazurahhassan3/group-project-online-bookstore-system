<?php 
include 'header.php';
require_once("db_connect.php");
$BOOK_ID =$_GET['ID'];
$query =" select * from books where BOOK_ID='".$BOOK_ID."'";
$result = mysqli_query($conn,$query);

while($row=mysqli_fetch_assoc($result))
{
                                        
                                        $BOOK_ID = $row['BOOK_ID'];
                                        $BOOK_TITLE = $row['BOOK_TITLE'];
                                        $BOOK_AUTHOR = $row['BOOK_AUTHOR'];
                                        $BOOK_PRICE = $row['BOOK_PRICE'];
                                        $BOOK_VERSION = $row['BOOK_VERSION'];
                                        $BOOK_PUBLISHDATE = $row['BOOK_PUBLISHDATE'];
                                        $BOOK_DESCRIPTION = $row['BOOK_DESCRIPTION'];
                                        $IMAGE_PATH = $row['IMAGE_PATH'];
                                        $CATEGORY_ID = $row['CATEGORY_ID'];}

?>




<div class="container">
    <div class="row">
        <div class="col-lg-6m-auto">
            <div class="card mt-5 ">
                <div class="card-title">
                    <h3 class="bg-secondary text-white  text-center py-3 px-2"> Manage Book</h3>
                </div>
                <div class="card-body px-2   ">

                    <form method="POST" action="update.php">

                        <input type="hidden" name=BOOK_ID value="<?php echo $BOOK_ID ?>">
                        <label for="BOOK_AUTHOR" class="form-label"><b>BOOK AUTHOR</b></label>
                        <input type="text" class="form-control mb-2" placeholder="BOOK_AUTHOR" name="BOOK_AUTHOR"
                            value="<?php echo $BOOK_AUTHOR ?>">
                        <label for="BOOK_TITLE" class="form-label"><b>BOOK TITLE</b></label>
                        <input type="text" class="form-control mb-2" placeholder="BOOK_TITLE" name="BOOK_TITLE"
                            value="<?php echo $BOOK_TITLE ?>">
                        <label for="BOOK_PRICE" class="form-label"><b>BOOK PRICE</b></label>
                        <input type="text" class="form-control mb-2" placeholder="BOOK_PRICE" name="BOOK_PRICE"
                            value="<?php echo $BOOK_PRICE ?>">
                        <label for="BOOK_VERSION" class="form-label"><b>BOOK VERSION</b></label>
                        <input type="text" class="form-control mb-2" placeholder="BOOK_VERSION" name="BOOK_VERSION"
                            value="<?php echo $BOOK_VERSION ?>">
                        <label for="label control-label"><b>BOOK PUBLISHDATE</b></label>
                        <input type="date" class="form-control mb-2" placeholder="BOOK_PUBLISHDATE"
                            name="BOOK_PUBLISHDATE" value="<?php echo $BOOK_PUBLISHDATE ?>">
                        <label for="BOOK_DESCRIPTION" class="form-label"><b>BOOK DESCRIPTION</b></label>
                        <textarea cols="30" rows="5" type="text" class="form-control mb-2"
                            placeholder="BOOK_DESCRIPTION"
                            name="BOOK_DESCRIPTION"><?php echo $BOOK_DESCRIPTION ?></textarea>
                        <label class="IMAGE_PATH" for="IMAGE_PATH"><b>IMAGE PATH</b></label>
                        <input type="file" class="form-control mb-2" name="IMAGE_PATH"
                            value="<?php echo 	$IMAGE_PATH ?>">
                        <label for="CATEGORY_ID" class="form-label"><b>CATEGORY ID</b></label>
                        <input type="text" class="form-control mb-2" placeholder="CATEGORY_ID" name="CATEGORY_ID"
                            value="<?php echo $CATEGORY_ID ?>">
                        <button class="btn btn-primary" name="edit">Edit</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>