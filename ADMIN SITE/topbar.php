<style>
.logo {
    margin: auto;
    font-size: 20px;
    background: white;
    padding: 7px 11px;
    border-radius: 50% 50%;
    color: #000000b3;
}
</style>

<nav class="navbar navbar-light fixed-top bg-info" style="padding:0">
    <div class="container-fluid mt-2 mb-2 md-2">
        <div class="col-lg-12">

            <div class="col- md-3 float-left text-white">
                <large><b>BOOK 2U Online Bookstore</b>
                </large>
            </div>
            <div class="float-right">
                <div class=" dropdown mr-5">
                    <a href="#" class="text-white dropdown-toggle" id="account_settings" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['username'] ?> </a>
                    <div class="dropdown-menu" aria-labelledby="account_settings" style="left: -2.5em;">
                        <a class="dropdown-item" href="index.php?page=manage_user" id="manage_my_account"><i
                                class="fa fa-cog"></i> Manage
                            Account</a>
                        <a class="dropdown-item" href="logout.php"><i class="fa fa-power-off"></i> Logout</a>
                    </div>
                </div>
            </div>
        </div>

</nav>
<script>
$(document).ready(function() {
    $(".dropdown-toggle").dropdown();
});
</script>