<?php include'db_connect.php';?>

<div class="container-fluid">

    <div class="col-lg-12">
        <div class="row">
            <!-- Table Panel -->
            <div class="col-md-12">
                <?php 

                    if(@$_GET['success']==true)
                    {
                    ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $_GET['success']?>
                </div>
                <?php }?>
                <div class="card">
                    <div class="card-header">
                        <b>ORDER LIST</b>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover">

                            <thead>
                                <tr>
                                    <th class="text-center">Order ID</th>
                                    <th class="text-center">Customer</th>
                                    <th class="text-center">Book ID</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
								
								$orders = $conn->query("SELECT *, cust_name FROM orders o join customer using(cust_id) ");
							
								while($row=$orders->fetch_assoc()):
									//$tamount = $conn->query("SELECT sum(book_price * book_qtty) as amount from order where order_id = ".$row['id'])->fetch_array()['amount'];
									//$items = $conn->query("SELECT sum(qty) as items from order_list where order_id = ".$row['id'])->fetch_array()['items'];
								?>


                                <tr>
                                    <td class="text-center"><?php echo ($row['ORDER_ID']) ?></td>

                                    </td>
                                    <td class="">
                                        <p><b><?php echo ($row['CUST_NAME']) ?></b></p>
                                    </td>
                                    <td class="text-center">
                                        <p><b><?php echo $row ['BOOK_ID'] ?></b></p>
                                    </td>

                                    <td class="text-center">

                                        <?php if($row['ORDER_STATUS'] == 'PENDING'): ?>
                                        <span class="badge badge-primary">PENDING</span>
                                        <?php elseif($row['ORDER_STATUS'] == 'DELIVERED'): ?>
                                        <span class="badge badge-success">DELIVERED</span>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <a href="index.php?page=manage_order&ID= <?php echo  $row['ORDER_ID'] ?>">
                                            <button class="btn btn-sm btn-primary edit_order">View</button> </a>
                                        <a href="delete_order.php?ORDER_ID= <?php echo$row['ORDER_ID'] ?>"><button
                                                class="btn btn-sm btn-danger delete_order"
                                                onclick='return checkdelete()'>Delete</button></a>
                                    </td>

                                </tr>
                                <?php endwhile;
								 ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Table Panel -->
        </div>
    </div>

</div>

<style>
td {
    vertical-align: middle !important;
}

td p {
    margin: unset;
}

.custom-switch {
    cursor: pointer;
}

.custom-switch * {
    cursor: pointer;
}

.img {
    max-height: 15vh;
}

/*.img img{
		max
	}*/
</style>

<script>
function checkdelete() {
    return confirm('Are you sure want to delete this data?');
}
</script>