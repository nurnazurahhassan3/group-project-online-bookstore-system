<nav id="sidebar" class='mx-lt bg-secondary'>

    <div class="sidebar-list ">
        <a href="index.php?page=home" class="nav-item nav-home pt-4 " style="height: 65px; font-size:medium; "><span
                class='icon-field'><i class="fa fa-tachometer-alt "></i></span> Dashboard</a>
        <a href="index.php?page=orders" class="nav-item nav-orders pt-3" style="height: 60px; font-size:medium;"><span
                class='icon-field'><i class="fa fa-clipboard-list"></i></span> Orders</a>
        <?php //if($_SESSION['login_type'] == 1): ?>
        <div class="mx-2 text-white "></div>
        <a href="index.php?page=categories" class="nav-item  nav-categories pt-3"
            style="height: 60px; font-size:medium;"><span class='icon-field'><i class="fa fa-list-alt "></i></span>
            Categories</a>
        <a href="index.php?page=books" class="nav-item nav-books pt-3" style="height: 60px;font-size:medium;"><span
                class='icon-field'><i class="fa fa-book "></i></span> Books</a>

    </div>

</nav>