<?php include('db_connect.php');?>

<div class="container-fluid">
    <?php 

if(@$_GET['success']==true)
{
?>
    <div class="alert alert-success" role="alert">
        <?php echo $_GET['success']?>
    </div>
    <?php }?>

    <div class="col-lg-12">
        <div class="row">

            <!-- FORM Panel -->
            <div class="col-md-4">
                <form action="categories_handler.php" id="manage-category" method="POST">
                    <div class="card">
                        <div class="card-header">
                            Category Form
                        </div>


                        <div class="card-body">

                            <div id="msg" class="form-group"></div>
                            <div class="form-group">
                                <label class="control-label">Category ID</label>
                                <input type="text" class="form-control" name="category_ID">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Category Name</label>
                                <input type="text" class="form-control" name="category_name">
                            </div>


                        </div>

                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-sm btn-primary col-sm-3 offset-md-3" value="submit">
                                        Save</button>
                                    <button class="btn btn-sm btn-default col-sm-3" type="button"
                                        onclick="$('#manage-category').get(0).reset()"> Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- FORM Panel -->

            <!-- Table Panel -->
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <b>Category List</b>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover">

                            <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Category Name</th>
                                    <th colspan='2' class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
								$i = 1;
								$category = $conn->query("SELECT * FROM categories");
								while($row=$category->fetch_assoc()):
								?>
                                <tr>
                                    <td class="text-center"><?php echo $row['CATEGORY_ID'] ?></td>
                                    <td class="">
                                        <p>Category Name: <b><?php echo $row['CATEGORY_NAME'] ?></b></p>

                                    </td>


                                    <td class="text-center  ">
                                        <button class="btn btn-sm btn-primary edit_category" type="button"
                                            data-id="<?php echo $row['CATEGORY_ID'] ?>"
                                            data-name="<?php echo $row['CATEGORY_NAME'] ?>">Edit</button>

                                    <td>

                                        <form method="post" action=" categories_handler.php">
                                            <input type="hidden" name="delete" value=<?php echo $row['CATEGORY_ID'] ?>>

                                            <button class=" btn btn-sm btn-danger delete_category"
                                                onclick='return checkdelete()' type="submit">Delete</button></a>
                                        </form>
                                    </td>
                                    </td>
                                </tr>
                                <?php endwhile; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Table Panel -->
        </div>
    </div>

</div>
<style>
td {
    vertical-align: middle !important;
}

td p {
    margin: unset;
}
</style>
<script>
$('.edit_category').click(function() {

    var cat = $('#manage-category')
    cat.get(0).reset()
    cat.find("[name='category_ID']").val($(this).attr('data-id'))
    cat.find("[name='category_name']").val($(this).attr('data-name'))

})

function checkdelete() {
    return confirm('Are you sure want to delete this data?');
}
</script>