<?php
session_start();
// initializing variables
$username = "";
$email    = "";
$errors = array(); 

// connect to the database
//$db = mysqli_connect('localhost:3305', 'root', '', 'book_ordering_db');
$db = mysqli_connect('localhost:3305', 'root', '', 'book_ordering_db');

// REGISTER USER
if (isset($_POST['reg_user'])) {
  // receive all input values from the form
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
  $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($username)) { array_push($errors, "Username is required"); }
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if ($password_1 != $password_2) {
	array_push($errors, "The two passwords do not match");
  }

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM user WHERE USERNAME='$username' OR EMAIL='$email' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
  if ($user) { // if user exists
    if ($user['USERNAME'] === $username) {
      array_push($errors, "Username already exists");
    }

    if ($user['EMAIL'] === $email) {
      array_push($errors, "email already exists");
    }
  }

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
    $options = array("cost"=>4);
    $password = password_hash($password_1 , PASSWORD_BCRYPT,$options);

  	$query = "INSERT INTO user (USERNAME, EMAIL, PASSWORD) 
  			  VALUES('$username', '$email', '$password')";
  	mysqli_query($db, $query);
    $query = "SELECT * FROM user WHERE USERNAME='".$username."' AND PASSWORD='".$password."'  limit 1";
    $result = mysqli_query($db, $query);
    $row = mysqli_fetch_assoc($result);
    $_SESSION['login']=1;
    $_SESSION['user_id'] =$row['ID'];
  	$_SESSION['username'] = $username;
  	$_SESSION['success'] = "You are now logged in";
  	header('location: index.php');
  }
}



// LOGIN USER
if (isset($_POST['login_user'])) {
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $pass = mysqli_real_escape_string($db,$_POST['password']);
  
    if (empty($username)) {
        array_push($errors, "Username is required");
    }
    if (empty($pass)) {
        array_push($errors, "Password is required");
    }
  
    if (count($errors) == 0) {
      
        $query = "SELECT * FROM user WHERE USERNAME='".$username."'  limit 1";
        $result = mysqli_query($db, $query);
        $row = mysqli_fetch_assoc($result);
        
        if (mysqli_num_rows($result) == 1) {
          if(password_verify($pass,$row['PASSWORD'])){
          $_SESSION['login']=1;
          $_SESSION['user_id'] =$row['ID'];
          $_SESSION['username'] = $username;
          $_SESSION['success'] = "You are now logged in";
          header('location:index.php');
          }
          else{
            array_push($errors, "Wrong password combination");
          }
        }else {
            array_push($errors, "Wrong username/password combination");
        }
    }
  }
  
  ?>