<?php include 'db_connect.php';
include 'header.php';
?>

<div class="container-fluid">

    <div class="col-lg-12">
        <div class="row">
            <?php 

                if(@$_GET['success']==true)
                {
                ?>
            <div class="alert alert-success" role="alert">
                <?php echo $_GET['success']?>
            </div>
            <?php }?>

            <body>
                <!-- Modal -->
                <div class="modal fade" id="BookAddModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add New Book</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>

                            <div class="modal-body">

                                <form action="insertcode.php" method="POST">

                                    <div class="mb-3">
                                        <label for="BOOK_TITLE" class="form-label"><b>BOOK TITLE</b></label>
                                        <input type="text" class="form-control" name="BOOK_TITLE" id="BOOK_TITLE">
                                    </div>
                                    <div class="mb-3">
                                        <label for="BOOK_AUTHOR" class="form-label"><b>BOOK AUTHOR</b></label>
                                        <input type="text" class="form-control" name="BOOK_AUTHOR" id="BOOK_AUTHOR">
                                    </div>
                                    <div class="mb-3">
                                        <label for="BOOK_PRICE" class="form-label"><b>BOOK PRICE</b></label>
                                        <input type="text" class="form-control" name="BOOK_PRICE" id="BOOK_PRICE">
                                    </div>
                                    <div class="mb-3">
                                        <label for="BOOK_VERSION" class="form-label"><b>BOOK VERSION</b></label>
                                        <input type="text" class="form-control" name="BOOK_VERSION" id="BOOK_VERSION">
                                    </div>
                                    <div class="mb-3">
                                        <label for="label control-label"><b>BOOK PUBLISHDATE</b></label>
                                        <input type="date" class="form-control" name="BOOK_PUBLISHDATE"
                                            id="BOOK_PUBLISHDATE">
                                    </div>
                                    <div class="mb-3">
                                        <label for="BOOK_DESCRIPTION" class="form-label"><b>BOOK DESCRIPTION</b></label>
                                        <textarea class="form-control" name="BOOK_DESCRIPTION" id="BOOK_DESCRIPTION"
                                            cols="30" rows="6"></textarea>
                                    </div>
                                    <div class="input-group mb-3">
                                        <label class="IMAGE_PATH" for="IMAGE_PATH"><b>IMAGE</b></label>
                                        <input type="file" name="IMAGE_PATH" class="form-control" id="IMAGE_PATH">
                                    </div>
                                    <div class="mb-3">
                                        <label for="CATEGORY_ID" class="form-label"><b>CATEGORY ID</b></label>
                                        <input type="text" class="form-control" name="CATEGORIES" id="CATEGORIES">
                                    </div>
                                    <button type="submit" name="insertdata" class="btn btn-primary">Save
                                        Data</button>
                                </form>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="container-fluid">

                    <div class="col-md-12">
                        <div class="card">



                            <div class="card-header">
                                <b>
                                    Book List
                                </b>
                                <span class="float:right"><button type="button"
                                        class="btn btn-primary btn-sm col-sm-3 float-right" data-bs-toggle="modal"
                                        data-bs-target="#BookAddModal">Add Data
                                    </button></span>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered table-hover">
                                    <colgroup>
                                        <col width="5%">
                                        <col width="15%">
                                        <col width="30%">
                                        <col width="20%">
                                        <col width="15%">

                                    </colgroup>

                                    <thead>
                                        <tr>
                                            <th class="text-center">Book ID</th>
                                            <th class="text-center">IMG</th>
                                            <th class="text-center">Details</th>
                                            <th class="text-center">Price(RM)</th>
                                            <th colspan='2' class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
								
                                        $book = $conn->query("SELECT * from books order by book_id asc");
                                        while($row=$book->fetch_assoc()):
                                        ?>

                                        <tr>
                                            <td class="text-center"><?php echo $row['BOOK_ID'] ?></td>
                                            <td class="">
                                                <div class="d-flex w-100">
                                                    <div class="img-field mr-4 img-thumbnail rounded">
                                                        <img src="image/<?php echo $row['IMAGE_PATH'] ?>" alt=""
                                                            class="img-fluid rounded">
                                                    </div>
                                                </div>
                                            </td>

                                            <td class="">
                                                <p>Title: <b><?php echo $row['BOOK_TITLE'] ?></b></p>
                                                <p><small>Author: <b><?php echo $row['BOOK_AUTHOR'] ?></b></small></p>
                                                <p><small>Version: <b><?php echo $row['BOOK_VERSION'] ?></b></small></p>
                                                <p><small>Description:<b><?php echo $row['BOOK_DESCRIPTION'] ?></b></small>


                                            </td>
                                            <td class="">
                                                <p class="text-right">
                                                    <b><?php echo number_format($row['BOOK_PRICE'],2) ?></b>
                                                </p>
                                            </td>
                                            <td>
                                                <a href="index.php?page=edit&ID= <?php echo  $row['BOOK_ID'] ?>"><button
                                                        class="btn btn-sm btn-primary edit_order">Edit</button> </a>

                                                <a href="delete2.php?BOOK_ID= <?php echo$row['BOOK_ID'] ?>"
                                                    onclick='return checkdelete()'><button
                                                        class="btn btn-sm btn-danger delete_order">Deletet</button></a>
                                            </td>

                                        </tr>

                                        <?php endwhile; ?>
                                    </tbody>
                                </table>

                            </div>

                        </div>
                        <!-- Table Panel -->
                    </div>
                </div>

            </body>
        </div>
    </div>
</div>
<script>
function checkdelete() {
    return confirm('Are you sure want to delete this data?');
}
</script>