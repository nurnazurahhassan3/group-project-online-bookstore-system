<?php 
include('db_connect.php');
if(isset($_SESSION['user_id'])){
$user = $conn->query("SELECT * FROM user where id ='".$_SESSION['user_id']."'");
foreach($user->fetch_array() as $k =>$v){
	$meta[$k] = $v;
}
}
?>
<div class="container">
    <div class="row">
        <div class="col-lg-6m-auto">
            <div class="card mt-5 ">
                <div class="card-title">
                    <h3 class="bg-secondary text-white  text-center py-3 px-2"> Manage User</h3>
                </div>
                <div class="card-body px-2   ">
                    <?php 

                    if(@$_GET['success']==true)
                    {
                    ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $_GET['success']?>
                    </div>
                    <?php }?>
                    <form action="managehandler.php" id="manage-user" method="post">
                        <input type="hidden" name="id" value="<?php echo isset($meta['ID']) ? $meta['ID']: '' ?>">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" class="form-control"
                                value="<?php echo isset($meta['USERNAME']) ? $meta['USERNAME']: '' ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" name="email" id="email" class="form-control"
                                value="<?php echo isset($meta['EMAIL']) ? $meta['EMAIL']: '' ?>" required
                                autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" class="form-control" value=""
                                autocomplete="off">
                            <small><i>Leave this blank if you dont want to change the password.</i></small>

                        </div>
                        <div class="btn-save">

                            <button class="btn btn-sm btn-primary save_user" type="submit">Save</button>

                        </div>






                    </form>
                </div>
            </div>
        </div>
    </div>
</div>