<!DOCTYPE html>
<html>
<?php session_start(); ?>

<title>BOOK2U ONLINE BOOKSTORE</title>
<?php
 
  if(!isset($_SESSION['login']))

  header('location:login.php');
  include('header.php');
 
 // include('./auth.php');
 ?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

</head>

<style>
body {
    background: #80808050;
}

.modal-dialog.large {
    width: 80% !important;
    max-width: unset;

}

.modal-dialog.mid-large {
    width: 50% !important;
    max-width: unset;

}

#viewer_modal .btn-close {
    position: absolute;
    z-index: 999999;
    /*right: -4.5em;*/
    background: unset;
    color: white;
    border: unset;
    font-size: 27px;
    top: 0;
}

#viewer_modal .modal-dialog {

    width: 80%;
    max-width: unset;
    height: calc(90%);
    max-height: unset;
}


#viewer_modal .modal-content {
    background: black;
    border: unset;
    height: calc(100%);
    display: flex;
    align-items: center;
    justify-content: center;

}

#viewer_modal img,
#viewer_modal video {
    max-height: calc(100%);
    max-width: calc(100%);

}


span.select2-selection.select2-selection--single {
    min-height: 2rem;

}
</style>


<body>
    <?php include 'topbar.php' ?>
    <?php include 'navbar.php' ?>
    <div class="toast" id="alert_toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body text-white">
        </div>
    </div>

    <main id="view-panel">
        <p>Welcome dear <strong><?php echo $_SESSION['username']; ?></strong></p>
        <?php $page = isset($_GET['page']) ? $_GET['page'] :'home'; ?>
        <?php include $page.'.php' ?>


    </main>



</body>

</html>