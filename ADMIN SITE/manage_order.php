<?php include "db_connect.php" ?>
<?php 
                       
                       if(isset($_GET['ID'])){
                       $qry = $conn->query("SELECT o.*,c.* FROM orders o inner join customer c on c.CUST_ID = o.CUST_ID where o.order_id = '".$_GET['ID']."' ");
                       foreach($qry->fetch_array() as $k => $v){
                       $$k = $v;
                       }
                     

                    }
?>

<style type="text/css">
.pb-5 {
    padding-bottom: 70 !important;
}

.img-field {
    width: calc(45%);
    max-height: 55vh;
    overflow: hidden;
    display: flex;
    justify-content: center
}

.detail-field {
    width: calc(50%);
}

.amount-field {
    width: calc(25%);
    text-align: right;
    display: flex;
    align-items: center;
    justify-content: center;
}

.img-field img {
    max-width: 100%;
    max-height: 100%;
}

.qty-input {
    width: 75px;
    text-align: center;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
</style>
<div class="">
    <div class="row">
        <div class="col-lg-7 m-auto ">
            <div class="card rounded-3">
                <form action="update_order.php" method="post">
                    <div class="card-title ">
                        <h3 class="bg-secondary text-white  text-center py-3 px-2"> Manage Order</h3>
                    </div>

                    <div class="card-body">


                        <div class="col-lg-12">

                            <div class="col-md-12- pb-5 ">
                                <?php 
                            $qry2 = $conn->query("SELECT s.*,b.* FROM orders s inner join books b on b.BOOK_ID = s.BOOK_ID where s.ORDER_ID ='".$ORDER_ID."' ");

                            while($row=mysqli_fetch_assoc($qry2))
                            {
                                                                    
                                                                    $BOOK_ID = $row['BOOK_ID'];
                                                                    $BOOK_TITLE = $row['BOOK_TITLE'];
                                                                    $BOOK_AUTHOR = $row['BOOK_AUTHOR'];
                                                                    $BOOK_PRICE = $row['BOOK_PRICE'];
                                                                    $BOOK_VERSION = $row['BOOK_VERSION'];
                                                                    $BOOK_PUBLISHDATE = $row['BOOK_PUBLISHDATE'];
                                                                    $BOOK_DESCRIPTION = $row['BOOK_DESCRIPTION'];
                                                                    $IMAGE_PATH = $row['IMAGE_PATH'];
                                                                    $CATEGORY_ID = $row['CATEGORY_ID'];}
                            
                            ?>

                                <b class="text-muted">Orders</b>

                                <div class="d-flex w-100">
                                    <div class="img-field mr-4 img-thumbnail rounded">
                                        <img src="image/<?php echo $IMAGE_PATH ?>" alt="" class="img-fluid rounded">
                                    </div>

                                    <div class="detail-field">
                                        <p>Book: <b><?php echo $BOOK_TITLE	?></b></p>
                                        <p>Author: <b><?php echo $BOOK_AUTHOR ?></b></p>
                                        <p>Price: <b>RM <?php echo number_format($BOOK_PRICE,2) ?></b></p>
                                        <p>Quantity: <b><?php echo $BOOK_QTTY ?></b></p>
                                        <p>Total: <b class="amount">RM
                                                <?php echo number_format($BOOK_PRICE*$BOOK_QTTY,2) ?></b></p>
                                        <p>Delivery cost: <b class="amount">RM 5.00</b></p>
                                    </div>

                                </div>


                            </div>
                            <div class="col-md-12">

                                <b class="text-muted">Information</b>
                                <p>Customer:</p>
                                <p> <b><?php echo ucwords($CUST_NAME) ?></b> </p>
                                <p>Delivery Address: </p>
                                <p><b><?php echo ucwords($CUST_ADDRESS) ?></b></p>
                                <p>Total Amount Payable: <b><?php echo number_format($TOTAL_AMOUNT,2) ?></b></p>


                                <div class="form-group">
                                    <input type="hidden" value="<?php echo $ORDER_ID?>" name="ORDER_ID">
                                    <label for="" class="control-label">Status</label>
                                    <select name="ORDER_STATUS" id="" class="custom-select custom-select-sm">
                                        <option value="PENDING">Pending</option>
                                        <option value="DELIVERED">Confirmed</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id='submit'>Save</button>

                        <a class="btn btn-secondary" href="index.php?page=orders"> Cancel <a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>